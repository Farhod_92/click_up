package uzb.farhod.click_up.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uzb.farhod.click_up.entity.User;

import javax.persistence.Convert;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    boolean existsByEmail(String email);

    Optional<User> findByEmailCodeAndEmail(String emailCode, String email);
    Optional<User> findByEmail(String email);

    @Query(value = "select u.id, u.created_at, u.updated_at, u.account_non_expired, u.account_non_locked, u.color, u.credentials_non_expired, u.email, u.email_code, u.enabled, u.full_name, u.password, u.system_role_name, u.created_by_id, u.updated_by_id, u.avatar_id from workspace_user wu\n" +
            "join workspace_role wr on wr.id = wu.workspace_role_id\n" +
            "join usr u on u.id = wu.user_id\n" +
            "join workspace w on w.id = wr.workspace_id\n" +
            "where w.id= ?1 and wr.name= ?2", nativeQuery = true)
    List<User> findAllByWorkspaceIdAndWorkspaceRoleName(Long workspaceId, String roleGuest);
}
