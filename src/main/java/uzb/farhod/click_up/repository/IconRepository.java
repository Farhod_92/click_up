package uzb.farhod.click_up.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.click_up.entity.Icon;

public interface IconRepository extends JpaRepository<Icon, Long> {
}
