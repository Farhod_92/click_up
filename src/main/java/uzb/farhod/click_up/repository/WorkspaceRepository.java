package uzb.farhod.click_up.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uzb.farhod.click_up.entity.Workspace;

import java.util.List;
import java.util.UUID;

public interface WorkspaceRepository extends JpaRepository<Workspace, Long> {
    boolean existsByOwnerIdAndName(UUID ownerId, String name);

    @Query(value = "select * from workspace_user wu join workspace w on w.id = wu.workspace_id " +
            "where wu.user_id= ?1 ", nativeQuery = true)
    List<Workspace> findAllByUserId(UUID id);

}
