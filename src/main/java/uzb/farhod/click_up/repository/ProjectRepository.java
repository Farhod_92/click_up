package uzb.farhod.click_up.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.click_up.entity.Project;

import java.util.List;

public interface ProjectRepository extends JpaRepository<Project, Long > {
    List<Project> findAllBySpaceId(Long space_id);
}
