package uzb.farhod.click_up.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;
import uzb.farhod.click_up.entity.WorkspaceUser;

import java.util.Optional;
import java.util.UUID;

public interface WorkspaceUserRepository extends JpaRepository<WorkspaceUser, Long> {
    Optional<WorkspaceUser> findByWorkspaceIdAndUserId(Long id, UUID id1);

    @Transactional
    @Modifying
    void deleteByWorkspaceIdAndUserId(Long id, UUID id1);

}
