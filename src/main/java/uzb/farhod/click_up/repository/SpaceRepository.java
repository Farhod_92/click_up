package uzb.farhod.click_up.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.click_up.entity.Space;

import java.util.List;

public interface SpaceRepository extends JpaRepository<Space, Long> {
    List<Space> findAllByWorkspaceId(Long workspace_id);
}
