package uzb.farhod.click_up.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;
import uzb.farhod.click_up.entity.ProjectUser;

import java.util.Optional;
import java.util.UUID;

public interface ProjectUserRepository extends JpaRepository<ProjectUser,Long > {
    @Transactional
    @Modifying
    int deleteByProjectIdAndUserId(Long project_id, UUID user_id);

    Optional<ProjectUser> findByProjectId(Long projectId);
}
