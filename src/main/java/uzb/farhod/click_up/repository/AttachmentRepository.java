package uzb.farhod.click_up.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.click_up.entity.Attachment;

import java.util.UUID;

public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {
}
