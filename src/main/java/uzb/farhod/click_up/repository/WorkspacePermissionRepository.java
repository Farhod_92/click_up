package uzb.farhod.click_up.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;
import uzb.farhod.click_up.entity.WorkspacePermission;
import uzb.farhod.click_up.entity.WorkspaceRole;
import uzb.farhod.click_up.entity.enums.WorkspacePermissionName;

import java.util.UUID;

public interface WorkspacePermissionRepository extends JpaRepository<WorkspacePermission, UUID> {
    @Transactional
    @Modifying
    int deleteByWorkspaceRoleIdAndPermission(UUID workspaceRoleId, WorkspacePermissionName permission);

    boolean existsByPermissionAndWorkspaceRole_Id(WorkspacePermissionName permission, UUID workspaceRole_id);

}
