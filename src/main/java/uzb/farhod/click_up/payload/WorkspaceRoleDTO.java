package uzb.farhod.click_up.payload;

import lombok.Getter;
import uzb.farhod.click_up.entity.enums.WorkspaceRoleName;

import javax.validation.constraints.NotNull;

@Getter
public class WorkspaceRoleDTO {

    @NotNull
    private Long workspaceId;

    @NotNull
    private String name;

    private WorkspaceRoleName extendsRole;
}
