package uzb.farhod.click_up.payload;

import lombok.Data;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Data
public class RegisterDto {
    @NotNull(message = "fullnameni kirgizing")
    private String fullName;

    @NotNull(message = "emailni kirgizing")
    private String email;

    @NotNull(message = "passwordni kirgizing")
    private String password;
}
