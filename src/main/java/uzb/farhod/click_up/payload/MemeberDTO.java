package uzb.farhod.click_up.payload;

import lombok.Getter;
import uzb.farhod.click_up.entity.enums.AddType;

import java.util.UUID;

@Getter
public class MemeberDTO {
    private UUID id;
    private UUID roleID;
    private AddType addType;
}
