package uzb.farhod.click_up.payload;

import lombok.Getter;
import uzb.farhod.click_up.entity.Attachment;
import uzb.farhod.click_up.entity.Space;

import javax.persistence.AccessType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
public class ProjectDTO {

    @NotNull
    private String name;

    @NotNull
    private Long spaceId;

    @NotNull
    private String accessType;

    @NotNull
    private boolean archived;

    @NotNull
    private String color;

    private UUID attachmentId;
}
