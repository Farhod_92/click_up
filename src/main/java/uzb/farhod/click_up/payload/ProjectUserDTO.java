package uzb.farhod.click_up.payload;

import lombok.Getter;
import uzb.farhod.click_up.entity.Project;
import uzb.farhod.click_up.entity.User;
import uzb.farhod.click_up.entity.enums.AddType;
import uzb.farhod.click_up.entity.enums.TaskPermission;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
public class ProjectUserDTO {

    @NotNull
    private UUID userId;

    private TaskPermission taskPermission;

    @NotNull
    private AddType addType;
}
