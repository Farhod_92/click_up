package uzb.farhod.click_up.payload;

import lombok.Getter;
import uzb.farhod.click_up.entity.enums.WorkspacePermissionName;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
public class WorkspacePermissionDTO {
    @NotNull
    private UUID workspaceRoleId;

    @NotNull
    private WorkspacePermissionName permission;
}
