package uzb.farhod.click_up.payload;

import lombok.Getter;
import uzb.farhod.click_up.entity.enums.AccessType;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
public class SpaceDTO {
    @NotNull
    private String name;

    @NotNull
    private String color;

    @NotNull
    private Long workspaceId;

    private Long iconId;

    private UUID avatarId;

    @NotNull
    private String accessType;

    private UUID ownerId;
}
