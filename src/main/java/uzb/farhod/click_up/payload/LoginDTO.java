package uzb.farhod.click_up.payload;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LoginDTO {
    @NotNull(message = "emailni kirgizing")
    private String email;

    @NotNull(message = "passwordni kirgizing")
    private String password;
}
