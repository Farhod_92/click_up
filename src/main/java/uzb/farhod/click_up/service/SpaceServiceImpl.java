package uzb.farhod.click_up.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uzb.farhod.click_up.entity.Space;
import uzb.farhod.click_up.entity.User;
import uzb.farhod.click_up.entity.Workspace;
import uzb.farhod.click_up.entity.WorkspaceUser;
import uzb.farhod.click_up.entity.enums.AccessType;
import uzb.farhod.click_up.entity.enums.WorkspacePermissionName;
import uzb.farhod.click_up.payload.ApiResponse;
import uzb.farhod.click_up.payload.SpaceDTO;
import uzb.farhod.click_up.repository.*;

import java.util.List;
import java.util.Optional;

@Service
public class SpaceServiceImpl implements SpaceService {
    @Autowired
    SpaceRepository spaceRepository;

    @Autowired
    WorkspaceRepository workspaceRepository;

    @Autowired
    IconRepository iconRepository;

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    WorkspacePermissionRepository workspacePermissionRepository;

    @Autowired
    WorkspaceUserRepository workspaceUserRepository;

    public ApiResponse addSpace(SpaceDTO spaceDTO, User user) {

        Optional<WorkspaceUser> optionalWorkspaceUser = workspaceUserRepository
                .findByWorkspaceIdAndUserId(spaceDTO.getWorkspaceId(), user.getId());
        if(!optionalWorkspaceUser.isPresent())
            return new ApiResponse("siz bu workspaceda ishlay olmaysiz", false);

        boolean exists=workspacePermissionRepository.existsByPermissionAndWorkspaceRole_Id(
                WorkspacePermissionName.CAN_CREATE_SPACES,
                optionalWorkspaceUser.get().getWorkspaceRole().getId());
        if(!exists)
            return new ApiResponse("siz bu workspacega space qo'sha olmaysiz", false);

        Space space=new Space();
        space.setName(spaceDTO.getName());
        space.setColor(spaceDTO.getColor());
        space.setWorkspace(workspaceRepository.getById(spaceDTO.getWorkspaceId()));

        if(spaceDTO.getIconId()!=null)
            space.setIcon(iconRepository.findById(spaceDTO.getIconId()).orElseThrow(() -> new ResourceNotFoundException("ICON_ID")));
        if(spaceDTO.getAvatarId()!=null)
            space.setAvatar(attachmentRepository.findById(spaceDTO.getAvatarId()).orElseThrow(() -> new ResourceNotFoundException("AVATAR_ID")));
        space.setOwner(user);
        space.setAccessType(AccessType.valueOf(spaceDTO.getAccessType()));
        spaceRepository.save(space);
        return new ApiResponse("yangi space qo'shildi", true);
    }

    @Override
    public List<Space> viewSpaces(Long workspaceId) {
        return spaceRepository.findAllByWorkspaceId(workspaceId);
    }

    @Override
    public ApiResponse editSpace(Long workspaceId, SpaceDTO spaceDTO, User user) {
        Optional<Space> optionalSpace = spaceRepository.findById(workspaceId);
        if(!optionalSpace.isPresent())
            return new ApiResponse("space topilmadi", false);

        Space space= optionalSpace.get();
        if(!space.getOwner().equals(user))
            return new ApiResponse("siz bu spaceni o'zgartirolmaysiz", false);

        space.setName(spaceDTO.getName());
        space.setColor(spaceDTO.getColor());
        if(spaceDTO.getWorkspaceId()!=null)
            space.setWorkspace(workspaceRepository.findById(spaceDTO.getWorkspaceId()).orElseThrow(() -> new ResourceNotFoundException("workspaceId")));
        if(spaceDTO.getIconId()!=null)
            space.setIcon(iconRepository.findById(spaceDTO.getIconId()).orElseThrow(() -> new ResourceNotFoundException("ICON_ID")));
        if(spaceDTO.getAvatarId()!=null)
            space.setAvatar(attachmentRepository.findById(spaceDTO.getAvatarId()).orElseThrow(() -> new ResourceNotFoundException("AVATAR_ID")));
        if(spaceDTO.getOwnerId()!=null)
            space.setOwner(userRepository.findById(spaceDTO.getOwnerId()).orElseThrow(() -> new ResourceNotFoundException("ownerId")));

        space.setAccessType(AccessType.valueOf(spaceDTO.getAccessType()));
        spaceRepository.save(space);
        return new ApiResponse(" space tahrirlandi", true);
    }

    @Override
    public ApiResponse deleteSpace(Long spaceId, User user) {
        Optional<Space> optionalSpace = spaceRepository.findById(spaceId);
        if(!optionalSpace.isPresent())
            return new ApiResponse("space topilmadi", false);
        if(optionalSpace.get().getOwner()!=user)
            return new ApiResponse("siz bu spaceni o'chirolmaysiz", false);

        try{
            spaceRepository.deleteById(spaceId);
        }catch (Exception e){
            return new ApiResponse("xatolik", false);
        }

        return new ApiResponse("space deleted", true);
    }
}
