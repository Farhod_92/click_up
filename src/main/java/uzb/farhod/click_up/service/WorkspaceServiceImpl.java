package uzb.farhod.click_up.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import uzb.farhod.click_up.entity.*;
import uzb.farhod.click_up.entity.enums.AddType;
import uzb.farhod.click_up.entity.enums.WorkspacePermissionName;
import uzb.farhod.click_up.entity.enums.WorkspaceRoleName;
import uzb.farhod.click_up.payload.*;
import uzb.farhod.click_up.repository.*;

import java.sql.Timestamp;
import java.util.*;

@Service
public class WorkspaceServiceImpl implements WorkspaceService {
    @Autowired
    WorkspaceRepository workspaceRepository;

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    WorkspaceUserRepository workspaceUserRepository;

    @Autowired
    WorkspaceRoleRepository workspaceRoleRepository;

    @Autowired
    WorkspacePermissionRepository workspacePermissionRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public ApiResponse addWorkspace(WorkspaceDTO workspaceDTO, User user) {
        //Workspace ochdik
        if(workspaceRepository.existsByOwnerIdAndName(user.getId(), workspaceDTO.getName()))
            return new ApiResponse("sizda bunaqa workspace bor", false);

        Workspace workspace=new Workspace(
                workspaceDTO.getName(),
                workspaceDTO.getColor(),
                user,
                workspaceDTO.getAvatarId()==null?null:attachmentRepository.findById(workspaceDTO.getAvatarId()).orElseThrow(() -> new ResourceNotFoundException("avatar topilmadi")));
        workspaceRepository.save(workspace);
        //Workspace role ochdik
        WorkspaceRole ownerRole = workspaceRoleRepository.save(new WorkspaceRole(
                workspace,
                WorkspaceRoleName.ROLE_OWNER.name(),
                null
        ));

        WorkspaceRole adminRole = workspaceRoleRepository.save(new WorkspaceRole(
                workspace,
                WorkspaceRoleName.ROLE_ADMIN.name(),
                null
        ));

        WorkspaceRole memberRole = workspaceRoleRepository.save(new WorkspaceRole(
                workspace,
                WorkspaceRoleName.ROLE_MEMBER.name(),
                null
        ));

        WorkspaceRole guestRole = workspaceRoleRepository.save(new WorkspaceRole(
                workspace,
                WorkspaceRoleName.ROLE_GUEST.name(),
                null
        ));
        //ROLLARGA huquqlar beramiz
        WorkspacePermissionName[] workspacePermissionNames=WorkspacePermissionName.values();

        List<WorkspacePermission> workspacePermissionList=new ArrayList<>();
        for (WorkspacePermissionName permissionName : workspacePermissionNames) {
            workspacePermissionList.add(new WorkspacePermission(ownerRole,permissionName));
            if(permissionName.getWorkspaceRoleNames().contains(WorkspaceRoleName.ROLE_ADMIN))
                workspacePermissionList.add(new WorkspacePermission(adminRole,permissionName));
            if(permissionName.getWorkspaceRoleNames().contains(WorkspaceRoleName.ROLE_MEMBER))
                workspacePermissionList.add(new WorkspacePermission(memberRole,permissionName));
            if(permissionName.getWorkspaceRoleNames().contains(WorkspaceRoleName.ROLE_GUEST))
                workspacePermissionList.add(new WorkspacePermission(guestRole,permissionName));
        }
       workspacePermissionRepository.saveAll(workspacePermissionList);
        //Workspace user ochdik
        workspaceUserRepository.save(new WorkspaceUser(
                workspace,
                user,
                ownerRole,
                new Timestamp(System.currentTimeMillis()),
                new Timestamp(System.currentTimeMillis()) ));
        return new ApiResponse("ishxona saqlandi" , true);
    }

    @Override
    public ApiResponse editWorkspace(Long id, WorkspaceDTO workspaceDTO, User user) {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findById(id);
        if(!optionalWorkspace.isPresent())
            return new ApiResponse("workspace topilmadi", false);
        Workspace workspace= optionalWorkspace.get();

        if(!workspace.getOwner().equals(user))
            return new ApiResponse("siz bu workspaceni o'zgartirolmaysiz", false);

        workspace.setName(workspaceDTO.getName());
        workspace.setColor(workspaceDTO.getColor());
        if(workspaceDTO.getAvatarId()!=null)
            workspace.setAvatar(attachmentRepository.findById(workspaceDTO.getAvatarId()).orElseThrow(() -> new ResourceNotFoundException("attachment topilmadi")));
        workspaceRepository.save(workspace);
        return new ApiResponse("workspace tahrirlandi", true);
    }

    @Override
    public ApiResponse changeOwnerWorkspace(Long id, UUID ownerId, User user) {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findById(id);
        if(!optionalWorkspace.isPresent())
            return new ApiResponse("workspace topilmadi", false);
        Workspace workspace= optionalWorkspace.get();

        if(!workspace.getOwner().equals(user))
            return new ApiResponse("siz bu workspace ownerini o'zgartirolmaysiz", false);

        workspace.setOwner(userRepository.findById(ownerId).orElseThrow(() -> new UsernameNotFoundException("user topilmadi")));
        workspaceRepository.save(workspace);
        return new ApiResponse("workspace owneri o'zgartirildi", true);
    }

    @Override
    public ApiResponse deleteWorkspace(Long id) {
        try{
            workspaceRepository.deleteById(id);
            return new ApiResponse("workspace o'chirildi", true);
        }catch (Exception e){
            return new ApiResponse("xatolik", false);
        }
    }

    @Override
    public ApiResponse addOrEditOrRemove(Long id, MemeberDTO memeberDTO) {
        if(memeberDTO.getAddType().equals(AddType.ADD)){
            WorkspaceUser workspaceUser=new WorkspaceUser(
                    workspaceRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("id")),
                    userRepository.findById(memeberDTO.getId()).orElseThrow(() -> new ResourceNotFoundException("userId")),
                    workspaceRoleRepository.findById(memeberDTO.getRoleID()).orElseThrow(() -> new ResourceNotFoundException("roleId")),
                    new Timestamp(System.currentTimeMillis()),
                    null
            );
            workspaceUserRepository.save(workspaceUser);
            //TODO EMAILGA INVITE XABAR YUBORISH
        }else if(memeberDTO.getAddType().equals(AddType.EDIT)){
            WorkspaceUser workspaceUser=workspaceUserRepository.findByWorkspaceIdAndUserId(id, memeberDTO.getId()).orElseGet(WorkspaceUser::new);
            workspaceUser.setWorkspaceRole(workspaceRoleRepository.findById(memeberDTO.getRoleID()).orElseThrow(() -> new ResourceNotFoundException("roleId")));
            workspaceUserRepository.save(workspaceUser);
        }else if(memeberDTO.getAddType().equals(AddType.REMOVE)){
            workspaceUserRepository.deleteByWorkspaceIdAndUserId(id, memeberDTO.getId());
        }
        return new ApiResponse("success " + memeberDTO.getAddType(), true);
    }

    @Override
    public ApiResponse joinToWorkspace(Long id, User user) {
        Optional<WorkspaceUser> optionalWorkspaceUser = workspaceUserRepository.findByWorkspaceIdAndUserId(id, user.getId());
        if(optionalWorkspaceUser.isPresent()){
            WorkspaceUser workspaceUser= optionalWorkspaceUser.get();
            workspaceUser.setDateJoined(new Timestamp(System.currentTimeMillis()));
            workspaceUserRepository.save(workspaceUser);
            return new ApiResponse("Success", true);
        }
        return new ApiResponse("didn't join", false);
    }

//    @Override
//    public ApiResponse viewMembersOrGuests(Long workspaceId, User user) {
//        Optional<Workspace> optionalWorkspace = workspaceRepository.findById(workspaceId);
//        if(!optionalWorkspace.isPresent())
//            return new ApiResponse("workspace topilmadi", false);
//        Workspace workspace= optionalWorkspace.get();
//
//        if(!workspace.getOwner().equals(user))
//            return new ApiResponse("siz bu workspace memberlarini ko'ra olmaysiz", false);
//
//        Map<String, List<User>> memberMap=new HashMap<>();
//        List<User> userList;
//           userList=userRepository.findAllByWorkspaceIdAndWorkspaceRoleName(workspaceId, WorkspaceRoleName.ROLE_GUEST.name());
//           memberMap.put("GUESTS", userList);
//           userList.clear();
//           userList=userRepository.findAllByWorkspaceIdAndWorkspaceRoleName(workspaceId, WorkspaceRoleName.ROLE_MEMBER.name());
//           memberMap.put("MEMBERS", userList);
//
//        return new ApiResponse(workspace.getName()+ " workspace userlar ro'yxati", true, memberMap);
//    }


    @Override
    public List<MemeberDTO> viewMembersOrGuests(Long workspaceId, User user) {
        return null;
    }

    @Override
    public List<Workspace> viewWorkspaces(User user) {
     return workspaceRepository.findAllByUserId(user.getId());
    }

    @Override
    public ApiResponse addRoleToWorkspace(WorkspaceRoleDTO workspaceRoleDTO, User user) {
        Optional<Workspace> optionalWorkspace = workspaceRepository.findById(workspaceRoleDTO.getWorkspaceId());
        if(!optionalWorkspace.isPresent())
            return new ApiResponse("workspace topilmadi", false);
        Workspace workspace= optionalWorkspace.get();

        if(!workspace.getOwner().equals(user))
            return new ApiResponse("siz bu workspacega rol  qo'sha olmaysiz", false);

        WorkspaceRole workspaceRole =new WorkspaceRole(workspace, workspaceRoleDTO.getName(), workspaceRoleDTO.getExtendsRole());
        workspaceRoleRepository.save(workspaceRole);
        return new ApiResponse("rol qo'shildi" , true);
    }

    @Override
    public ApiResponse addPermissionToRole(WorkspacePermissionDTO workspacePermissionDTO, User user) {
        Optional<WorkspaceRole> optionalWorkspaceRole = workspaceRoleRepository.findById(workspacePermissionDTO.getWorkspaceRoleId());
        if(!optionalWorkspaceRole.isPresent())
            return new ApiResponse("workspace topilmadi", false);
        WorkspaceRole workspaceRole= optionalWorkspaceRole.get();

        if(!workspaceRole.getWorkspace().getOwner().equals(user))
            return new ApiResponse("siz bu rolning permissionlarini o'zgartirolmaysiz", false);

        WorkspacePermission workspacePermission=new WorkspacePermission(
                workspaceRole,
                workspacePermissionDTO.getPermission()
                );
        workspacePermissionRepository.save(workspacePermission);
        return new ApiResponse("rolega yangi permission qo'shildi", true);
    }

    @Override
    public ApiResponse deletePermissionFromRole(WorkspacePermissionDTO workspacePermissionDTO, User user) {
        Optional<WorkspaceRole> optionalWorkspaceRole = workspaceRoleRepository.findById(workspacePermissionDTO.getWorkspaceRoleId());
        if(!optionalWorkspaceRole.isPresent())
            return new ApiResponse("workspace topilmadi", false);
        WorkspaceRole workspaceRole= optionalWorkspaceRole.get();

        if(!workspaceRole.getWorkspace().getOwner().equals(user))
            return new ApiResponse("siz bu rolning permissionlarini o'zgartirolmaysiz", false);

        int b = workspacePermissionRepository.deleteByWorkspaceRoleIdAndPermission(
                workspacePermissionDTO.getWorkspaceRoleId(),
                workspacePermissionDTO.getPermission()
        );

        System.out.println("delete=" + b);
        if(b==1)
            return new ApiResponse("roledan permission o'chirildi", true);
        else
            return new ApiResponse("roledan permission o'chirilmadi", true);
    }
}
