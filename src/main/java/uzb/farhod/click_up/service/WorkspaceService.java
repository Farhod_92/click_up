package uzb.farhod.click_up.service;

import uzb.farhod.click_up.entity.User;
import uzb.farhod.click_up.entity.Workspace;
import uzb.farhod.click_up.payload.*;

import java.util.List;
import java.util.UUID;

public interface WorkspaceService {

    ApiResponse addWorkspace(WorkspaceDTO workspaceDTO, User user);

    ApiResponse editWorkspace(Long id, WorkspaceDTO workspaceDTO, User user);

    ApiResponse changeOwnerWorkspace(Long id, UUID ownerId, User user);

    ApiResponse deleteWorkspace(Long id);

    ApiResponse addOrEditOrRemove(Long id, MemeberDTO memeberDTO);

    ApiResponse joinToWorkspace(Long id, User user);

    List<MemeberDTO> viewMembersOrGuests(Long workspaceId, User user);

    List<Workspace> viewWorkspaces(User user);

    ApiResponse addRoleToWorkspace(WorkspaceRoleDTO workspaceRoleDTO, User user);

    ApiResponse addPermissionToRole(WorkspacePermissionDTO workspaceRoleDTO, User user);

    ApiResponse deletePermissionFromRole(WorkspacePermissionDTO workspacePermissionDTO, User user);
}
