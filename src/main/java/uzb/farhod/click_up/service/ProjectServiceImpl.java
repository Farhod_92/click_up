package uzb.farhod.click_up.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uzb.farhod.click_up.entity.*;
import uzb.farhod.click_up.entity.enums.AccessType;
import uzb.farhod.click_up.entity.enums.AddType;
import uzb.farhod.click_up.entity.enums.WorkspacePermissionName;
import uzb.farhod.click_up.payload.ApiResponse;
import uzb.farhod.click_up.payload.ProjectDTO;
import uzb.farhod.click_up.payload.ProjectUserDTO;
import uzb.farhod.click_up.repository.*;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    SpaceRepository spaceRepository;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    ProjectUserRepository projectUserRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    WorkspaceUserRepository workspaceUserRepository;

    @Autowired
    WorkspacePermissionRepository workspacePermissionRepository;

    @Override
    public ApiResponse addProject(ProjectDTO projectDTO, User user) {
        Optional<Space> optionalSpace = spaceRepository.findById(projectDTO.getSpaceId());
        if(!optionalSpace.isPresent())
            return new ApiResponse("space topilmadi", false);

        Space space = optionalSpace.get();

        Optional<WorkspaceUser> optionalWorkspaceUser = workspaceUserRepository
                .findByWorkspaceIdAndUserId(space.getWorkspace().getId(), user.getId());
        if(!optionalWorkspaceUser.isPresent())
            return new ApiResponse("siz bu workspaceda ishlay olmaysiz", false);

        boolean exists=workspacePermissionRepository.existsByPermissionAndWorkspaceRole_Id(
                WorkspacePermissionName.CAN_CREATE_FOLDER,
                optionalWorkspaceUser.get().getWorkspaceRole().getId());
        if(!exists)
            return new ApiResponse("siz bu workspacega folder qo'sha olmaysiz", false);

        Project project=new Project();
        project.setName(projectDTO.getName());
        project.setSpace(space);
        project.setAccessType(AccessType.valueOf(projectDTO.getAccessType()));
        project.setArchived(projectDTO.isArchived());
        project.setColor(projectDTO.getColor());
        if(projectDTO.getAttachmentId()!=null)
            project.setAttachment(attachmentRepository.findById(projectDTO.getAttachmentId()).orElseThrow(() ->new ResourceNotFoundException("spaceId")));
        projectRepository.save(project);
        return new ApiResponse("project saqlandi", true);
    }

    @Override
    public List<Project> viewProjects(Long spaceId) {
        return projectRepository.findAllBySpaceId(spaceId);
    }

    @Override
    public ApiResponse editProject(Long projectId, ProjectDTO projectDTO, User user) {
        Optional<Project> optionalProject = projectRepository.findById(projectId);
        if(!optionalProject.isPresent())
            return new ApiResponse("project topilmadi", false);

        Project project= optionalProject.get();
        project.setName(projectDTO.getName());
        project.setSpace(spaceRepository.findById(projectDTO.getSpaceId()).orElseThrow(() ->new ResourceNotFoundException("spaceId")));
        project.setAccessType(AccessType.valueOf(projectDTO.getAccessType()));
        project.setArchived(projectDTO.isArchived());
        project.setColor(projectDTO.getColor());
        if(projectDTO.getAttachmentId()!=null)
            project.setAttachment(attachmentRepository.findById(projectDTO.getAttachmentId()).orElseThrow(() ->new ResourceNotFoundException("spaceId")));
        projectRepository.save(project);
        return new ApiResponse("project saqlandi", true);
    }

    @Override
    public ApiResponse deleteProject(Long projectId, User user) {
        Optional<Project> optionalProject = projectRepository.findById(projectId);
        if(!optionalProject.isPresent())
            return new ApiResponse("project topilmadi", false);

        try{
            projectRepository.deleteById(projectId);
        }catch (Exception e){
            return new ApiResponse("xatolik", false);
        }

        return new ApiResponse("project deleted", true);
    }

    @Override
    public ApiResponse addOrEditOrRemove(Long projectId, ProjectUserDTO projectUserDTO) {
        if(projectUserDTO.getAddType().equals(AddType.ADD)){
            ProjectUser projectUser=new ProjectUser(
                projectRepository.findById(projectId).orElseThrow(() -> new ResourceNotFoundException("project")),
                userRepository.findById(projectUserDTO.getUserId()).orElseThrow(() -> new ResourceNotFoundException("user")),
                projectUserDTO.getTaskPermission()
            );
            projectUserRepository.save(projectUser);
        }else if(projectUserDTO.getAddType().equals(AddType.EDIT)){
            Optional<ProjectUser> optionalProjectUser = projectUserRepository.findByProjectId(projectId);
            if(!optionalProjectUser.isPresent())
                return new ApiResponse("project user topilmadi", false);
            ProjectUser projectUser= optionalProjectUser.get();
            projectUser.setProject(projectRepository.findById(projectId).orElseThrow(() -> new ResourceNotFoundException("project")));
            projectUser.setUser(userRepository.findById(projectUserDTO.getUserId()).orElseThrow(() -> new ResourceNotFoundException("user")));
            projectUser.setTaskPermission(projectUserDTO.getTaskPermission());
            projectUserRepository.save(projectUser);
        }else if(projectUserDTO.getAddType().equals(AddType.REMOVE)){
            projectUserRepository.deleteByProjectIdAndUserId(projectId, projectUserDTO.getUserId());
        }
        return new ApiResponse("success " + projectUserDTO.getAddType(), true);
    }
}
