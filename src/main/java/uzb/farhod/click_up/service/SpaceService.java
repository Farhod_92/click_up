package uzb.farhod.click_up.service;

import uzb.farhod.click_up.entity.Space;
import uzb.farhod.click_up.entity.User;
import uzb.farhod.click_up.payload.ApiResponse;
import uzb.farhod.click_up.payload.SpaceDTO;

import java.util.List;

public interface SpaceService {
    ApiResponse addSpace(SpaceDTO spaceDTO, User user);

    List<Space> viewSpaces(Long workspaceId);

    ApiResponse editSpace(Long spaceId, SpaceDTO spaceDTO, User user);

    ApiResponse deleteSpace(Long spaceId, User user);
}
