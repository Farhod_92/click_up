package uzb.farhod.click_up.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uzb.farhod.click_up.entity.User;
import uzb.farhod.click_up.entity.enums.SystemRoleName;
import uzb.farhod.click_up.payload.ApiResponse;
import uzb.farhod.click_up.payload.RegisterDto;
import uzb.farhod.click_up.repository.UserRepository;

import java.util.Optional;
import java.util.Random;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    private  final UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JavaMailSender javaMailSender;


    public AuthService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("user topilmadi"));
    }

    public ApiResponse registerUser(RegisterDto registerDto) {

       if(userRepository.existsByEmail(registerDto.getEmail()))
            return new ApiResponse("email exists", false);

            User user=new User(
                    registerDto.getFullName(),
                    registerDto.getEmail(),
                    passwordEncoder.encode(registerDto.getPassword()),
                    SystemRoleName.SYSTEM_ROLE_USER
            );
        int code = new Random().nextInt(999999);
        user.setEmailCode(String.valueOf(code).substring(0,4));
        userRepository.save(user);
        sendEmail(user.getEmail(), user.getEmailCode());
        return new ApiResponse("mailga xabar jo'natildi", true);
    }

    public ApiResponse sendEmail(String sendingEmail, String emailCode){
        try{
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setFrom("f.x.1992@inbox.ru");
            mailMessage.setTo(sendingEmail);
            mailMessage.setSubject("Akkauntni tasdiqlash");
            mailMessage.setText("<a href='http:/localhost:8080/api/auth/verifyEmail?emailCode=" + emailCode + "&email=" + sendingEmail + "'>Tasqidlash</a>");

            javaMailSender.send(mailMessage);
            return new ApiResponse("sent",true);
        }catch (Exception e){
            return new ApiResponse(e.getMessage(),false);
        }
    }

    public ApiResponse verifyEmail(String emailCode, String email){
        Optional<User> optionalUser = userRepository.findByEmailCodeAndEmail(emailCode, email);
        if(optionalUser.isPresent()){
            User user= optionalUser.get();
            user.setEnabled(true);
            user.setEmailCode(null);
            userRepository.save(user);
            return new ApiResponse("akkaunt tasdiqlandi", true);
        }

        return new ApiResponse("akkaunt tasdiqlanmadi", false);
    }
}
