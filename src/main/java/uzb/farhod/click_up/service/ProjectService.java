package uzb.farhod.click_up.service;

import uzb.farhod.click_up.entity.Project;
import uzb.farhod.click_up.entity.User;
import uzb.farhod.click_up.payload.*;

import java.util.List;

public interface ProjectService {
    ApiResponse addProject(ProjectDTO projectDTO, User user);

    List<Project> viewProjects(Long spaceId);

    ApiResponse editProject(Long projectId, ProjectDTO projectDTO, User user);

    ApiResponse deleteProject(Long projectId, User user);

    ApiResponse addOrEditOrRemove(Long projectId, ProjectUserDTO memeberDTO);
}
