package uzb.farhod.click_up.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.click_up.entity.Space;
import uzb.farhod.click_up.entity.User;
import uzb.farhod.click_up.payload.ApiResponse;
import uzb.farhod.click_up.payload.SpaceDTO;
import uzb.farhod.click_up.security.CurrentUser;
import uzb.farhod.click_up.service.SpaceService;

import java.util.List;

@RestController
@RequestMapping("/api/space")
public class SpaceController {

    @Autowired
    SpaceService spaceService;

    @PostMapping
    public HttpEntity<?> addSpace(@RequestBody SpaceDTO spaceDTO, @CurrentUser User user){
        ApiResponse apiResponse=spaceService.addSpace(spaceDTO,user);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @GetMapping("/{workspaceId}")
    public HttpEntity<?> viewSpaces(@PathVariable Long workspaceId){
        List<Space> spaceList=spaceService.viewSpaces(workspaceId);
        return ResponseEntity.ok(spaceList);
    }

    @PutMapping("/{spaceId}")
    public HttpEntity<?> editSpace(@PathVariable Long spaceId, @RequestBody SpaceDTO spaceDTO, @CurrentUser User user){
        ApiResponse apiResponse=spaceService.editSpace(spaceId, spaceDTO, user);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @DeleteMapping("/{spaceId}")
    public HttpEntity<?> deleteSpace(@PathVariable Long spaceId, @CurrentUser User user){
        ApiResponse apiResponse=spaceService.deleteSpace(spaceId, user);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }


}
