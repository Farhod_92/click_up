package uzb.farhod.click_up.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.click_up.entity.Project;
import uzb.farhod.click_up.entity.Space;
import uzb.farhod.click_up.entity.User;
import uzb.farhod.click_up.payload.*;
import uzb.farhod.click_up.security.CurrentUser;
import uzb.farhod.click_up.service.ProjectService;
import uzb.farhod.click_up.service.SpaceService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/project")
public class ProjectController {

    @Autowired
    ProjectService projectService;

    @PostMapping
    public HttpEntity<?> addProject(@RequestBody ProjectDTO projectDTO, @CurrentUser User user){
        ApiResponse apiResponse=projectService.addProject(projectDTO,user);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @GetMapping("/{spaceId}")
    public HttpEntity<?> viewProjects(@PathVariable Long spaceId){
        List<Project> spaceList=projectService.viewProjects(spaceId);
        return ResponseEntity.ok(spaceList);
    }

    @PutMapping("/{projectId}")
    public HttpEntity<?> editProject(@PathVariable Long projectId, @RequestBody ProjectDTO projectDTO, @CurrentUser User user){
        ApiResponse apiResponse=projectService.editProject(projectId, projectDTO, user);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @DeleteMapping("/{projectId}")
    public HttpEntity<?> deleteProject(@PathVariable Long projectId, @CurrentUser User user){
        ApiResponse apiResponse=projectService.deleteProject(projectId, user);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @PostMapping("/addOrEditOrRemove/{projectId}")
    public HttpEntity<?> addOrEditOrRemove(@PathVariable Long projectId,@Valid @RequestBody ProjectUserDTO projectUserDTO){
        ApiResponse apiResponse=projectService.addOrEditOrRemove(projectId, projectUserDTO);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

}
