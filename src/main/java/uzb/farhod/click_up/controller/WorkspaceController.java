package uzb.farhod.click_up.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.click_up.entity.User;
import uzb.farhod.click_up.entity.Workspace;
import uzb.farhod.click_up.payload.*;
import uzb.farhod.click_up.security.CurrentUser;
import uzb.farhod.click_up.service.WorkspaceService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/workspace")
public class WorkspaceController {
    @Autowired
    WorkspaceService workspaceService;

    @PostMapping
    public HttpEntity<?> addWorkspace(@Valid @RequestBody WorkspaceDTO workspaceDTO, @CurrentUser User user){
        ApiResponse apiResponse=workspaceService.addWorkspace(workspaceDTO, user);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editWorkspace(@PathVariable Long id,  @RequestBody WorkspaceDTO workspaceDTO, @CurrentUser User user){
        ApiResponse apiResponse=workspaceService.editWorkspace(id,workspaceDTO, user);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @PutMapping("/changeOwner/{id}")
    public HttpEntity<?> chanegeOwnerWorkspace(@PathVariable Long id, @RequestParam UUID ownerId, @CurrentUser User user){
        ApiResponse apiResponse=workspaceService.changeOwnerWorkspace(id, ownerId, user);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }


    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteWorkspace(@PathVariable Long id){
        ApiResponse apiResponse=workspaceService.deleteWorkspace(id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @PostMapping("/addOrEditOrRemove/{id}")
    public HttpEntity<?> addOrEditOrRemove(@PathVariable Long id,@Valid @RequestBody MemeberDTO memeberDTO){
        ApiResponse apiResponse=workspaceService.addOrEditOrRemove(id, memeberDTO);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @PutMapping("/join")
    public HttpEntity<?> joinToWorkspace(@RequestParam Long id, @CurrentUser User user){
        ApiResponse apiResponse=workspaceService.joinToWorkspace(id, user);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @GetMapping("/viewMembers/{workspaceId}")
    public HttpEntity<?> viewMembersOrGuests(@PathVariable Long workspaceId, @CurrentUser User user){
       List<MemeberDTO> memeberDTOS = workspaceService.viewMembersOrGuests(workspaceId, user);
        return ResponseEntity.ok(memeberDTOS);
    }

    @GetMapping("/viewWorkspaces")
    public HttpEntity<?> viewWorkspaces(@CurrentUser User user){
        List<Workspace> workspaceList = workspaceService.viewWorkspaces(user);
        return ResponseEntity.status(workspaceList.isEmpty()?409:200).body(workspaceList);
    }

    @PostMapping("/addRoleToWorkspace")
    public HttpEntity<?> addRoleToWorkspace(@Valid @RequestBody WorkspaceRoleDTO workspaceRoleDTO, @CurrentUser User user){
        ApiResponse apiResponse=workspaceService.addRoleToWorkspace(workspaceRoleDTO, user);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @PutMapping("/addPermissionToRole")
    public HttpEntity<?> addPermissionToRole(@Valid @RequestBody WorkspacePermissionDTO workspacePermissionDTO, @CurrentUser User user){
        ApiResponse apiResponse=workspaceService.addPermissionToRole(workspacePermissionDTO, user);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @DeleteMapping("/deletePermissionFromRole")
    public HttpEntity<?> deletePermissionFromRole(@Valid @RequestBody WorkspacePermissionDTO workspacePermissionDTO, @CurrentUser User user){
        ApiResponse apiResponse=workspaceService.deletePermissionFromRole(workspacePermissionDTO, user);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }



}
