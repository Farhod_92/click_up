package uzb.farhod.click_up.security;

import org.springframework.stereotype.Component;
import io.jsonwebtoken.*;

import java.util.Date;

@Component
public class JwtProvider {
    private String secretKey="secretlykey";
    private long expireAfter=1000*60*60*24;

    public String generateToken(String username){
        String token = Jwts.builder()
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + expireAfter))
                .setSubject(username)
                .compact();
        return token;
    }

    public String getUsernameFromToken(String token){
        String username = Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        return username;
    }

}
