package uzb.farhod.click_up.entity.enums;

public enum StatusType {
    OPEN,
    CUSTOM,
    CLOSED
}
