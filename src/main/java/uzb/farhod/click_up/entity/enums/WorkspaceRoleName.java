package uzb.farhod.click_up.entity.enums;

public enum WorkspaceRoleName {
    ROLE_OWNER,
    ROLE_ADMIN,
    ROLE_GUEST,
    ROLE_MEMBER
}
