package uzb.farhod.click_up.entity.enums;

public enum DependencyType {
    WAITING,
    BLOCKING,
    LINKS
}
