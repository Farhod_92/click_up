package uzb.farhod.click_up.entity.enums;

public enum TaskPermission {
    CREATE,
    EDIT,
    DELETE,
    READ
}
