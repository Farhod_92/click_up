package uzb.farhod.click_up.entity.enums;

public enum AddType {
    ADD,
    REMOVE,
    EDIT
}
