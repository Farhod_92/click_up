package uzb.farhod.click_up.entity.enums;

public enum AccessType {
    PRIVATE,
    PUBLIC
}
