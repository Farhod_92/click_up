package uzb.farhod.click_up.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uzb.farhod.click_up.entity.enums.AccessType;
import uzb.farhod.click_up.entity.template.AbsLongEntity;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Project extends AbsLongEntity {

    private String name;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Space space;

    @Enumerated
    private AccessType accessType;

    private boolean archived;

    private String color;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Attachment attachment;

}
