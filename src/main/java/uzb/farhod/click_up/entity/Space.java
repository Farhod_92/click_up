package uzb.farhod.click_up.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uzb.farhod.click_up.entity.enums.AccessType;
import uzb.farhod.click_up.entity.template.AbsLongEntity;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Space extends AbsLongEntity {

    private String name;

    private String color;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Workspace workspace;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Icon icon;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Attachment avatar;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private User owner;

    @Enumerated(EnumType.STRING)
    private AccessType accessType;

    @Column(nullable = false)
    private String initialLetter;

    @PrePersist
    @PreUpdate
    private void  setInitialLetterFromName(){
        this.initialLetter=name.substring(0,1).toUpperCase();
    }

    public Space(String name, String color, Workspace workspace, Icon icon, Attachment avatar, User owner, AccessType accessType) {
        this.name = name;
        this.color = color;
        this.workspace = workspace;
        this.icon = icon;
        this.avatar = avatar;
        this.owner = owner;
        this.accessType = accessType;
    }

//    @Transient
//    private String initialLetter;
    //    public String getInitialLetter() {
//        String[] s = name.split(" ");
//        for (String s1 : s) {
//            initialLetter+=s1.charAt(0);
//        }
//        return initialLetter.toUpperCase();
//    }

}
